# Análisis de tráfico de red y estrategias de enrutamiento
-----

# Indice 
1. Resumen

2. Introducción

3. Analisis de una Red con topología de anillo simple

4. Algoritmo de enrutamiento

5. Conclusión

## Resumen

En este trabajo se va a tratar de resolver uno de los tantos problemas que poseen las redes hoy en día. Se basará en una red con topologia de anillo simple, diseñada y simulada en Omnet++ y ademas se propondrá un algoritmo de enrutamiento.


## Introducción

La capa de red es la encargada de llevar los paquetes un host de origen a uno de destino siguiendo una ruta conveniente. Ademas es encargada de almacenar, reenvíar y enrutar paquetes (entre otros) pero este trabajo pondrá foco en estos asuntos.

Se simularan varios casos de estudio y obtendrán
conclusiones sobre el trafico de la red. En el primer caso de estudio los *nodos 0 y 2* enviaran paquetes a un *nodo 5* y para el segundo caso de estudio el *nodo 5* sigue siendo el unico destino pero todos los demas nodos son generadores.

## Analisis de una Red con topología de anillo
Trabajaremos con un modelo de red en anillo que consta de *8 nodos*, cada uno con
dos interfaces de comunicación full-duplex con dos posibles vecinos.

![chart](./images/red.png)
Internamente, cada nodo cuenta con dos capas de enlace (link o lnk, una con cada vecino),
una capa de red (net) y una capa de aplicación (app). La capa de aplicación y la capa de
enlace implementan generadores de tráfico y buffers respectivamente.

### Caso de estudio 1

Las configuraciones de este caso de estudio son las siguientes 

1. Los nodos 0 y 2 son generadores
2. El nodo 5 es el único destino 
3. *Buffersize* de 125KB
4. Intervalo de generación **exponential(1)**

La cantidad de paquetes generados fueron 390, y el 63.4% llegaron al destino.
Ademas queda reflejado que el intervalo de cantidad de saltos es de [3, 5].
Cabe aclarar que el punto de congestión se encuentra dado en el *nodo 0* el cual contiene una cantidad superior de paquetes que la cola del *nodo 2* (Figura 1). Esto se debe a
que el *nodo 0* no sólo debe recibir y procesar los paquetes que vienen a su izquierda (los generados por el *nodo 2*), si no que tambien debe generar paquetes. Como resultado la cola del *nodo 0* aumenta rápidamente.

#### Figura 1 - Buffer size de los nodos
![buffersize](./images/sin-algoritmo/caso-1/buffersize.png)

Es notable que hay un mal uso de recursos en la red, ya que, la gran mayoria de los nodos
se encuentran descansando y todo trafico de la misma cae en un solo nodo.
El mal uso de recursos es mejorable, y lo demostraremos cambiando el algoritmo de enrutamiento.

### Caso de estudio 2

Las configuraciones de este caso de estudio son las siguientes 

1. Los nodos 0,1,2,3,4,6 y 7 son generadores
2. El nodo 5 es el único destino 
3. *Buffersize* de 125KB
4. Intervalo de generación **exponential(1)**

En este caso, la cantidad de paquetes generados fueron 1378, y solo el 14.44% llegaron al destino.
A diferencia del caso de estudio 1, el intervalo de cantidad de saltos es [1, 7], ya que el nodo que esté justo a la izquierda del nodo 5, va a generar los paquetes y enviarlos directos al destino en un solo salto, y el nodo que este ubicado a la derecha del nodo 5 (es decir el nodo 4) tiene que enviar los paquetes y pasar por todos los demás nodos, hasta llegar al destino.


#### Grafico 2 - Buffer size de los nodos
![buffersize](./images/sin-algoritmo/caso-2/buffersize.png)

Gracias a este grafico podemos observar que el unico nodo no saturado es el *Nodo 4*, ya que es el unico que no trabaja como emisor y receptor al mismo tiempo, y solo genera paquetes y los transmite al *Nodo 3*. 

Se puede observar que la cantidad de paquetes en los buffers de todos los nodos, salvo el 4, esta muy llena, y quedaron muchos paquetes en transito al finalizar la simulacion, por eso solo llega un 5.05% de los todos los paquetes generados llegaron al destino.

Podemos encontrar que la red se estabiliza con un interArrivalTime de aproximadamente **exponential(7)**, ya que con esta configuracion, casi un 100% de los paquetes generados fueron recibidos por el *nodo 5*, esto se debe a que el tiempo de generacion entre paquetes es mas grande que el tiempo que tardan los nodos en procesar y enrutar los paquetes recibidos

## Algoritmo de enrutamiento basado en vectores de distancia

El algotirmo se basa en vector distancia, pero el costo en vez de ser el delay es la cantidad de saltos que tiene que realizar un paquete para llegar a destino. Por otro lado este algoritmo está diseñado para soportar una cantidad máxima de 256 nodos en la red. Al principio cada nodo le envía un **Hello** a todos sus vecinos para saber quienes son, posterirormente envía periodicamente la tabla que tiene a cada  vecino para que ellos puedan actualizar las suyas con el camino más óptimo. Finalmente cada un segundo cada nodo verifica la existencia de sus vecinos para evitar enviar mensajes por caminos inexistentes. 

### Caso de Estudio 1
Cabe señalar antes de comenzar el análisis, que con el algoritmo no podemos hacer comparaciones de metricas directamente contra el algoritmo de enrutamiento planteado por el kickstarter. 
¿Porque?
Se debe a que en el **Caso de Estudio 1** en la sección 3, se puede apreciar como
se congestiona el *nodo 0* al tener que procesar y generar paquetes datos.
Ahora bien, a la hora de usar el algoritmo en la red no solo viajan paquetes de datos, tambien nos encontramos con paquetes *hello* y la tabla de vecinos, todos estos, son almacenados en un mismo buffer entonces no es posible comparados porque tienen datos de mas.

Sin embarlo como nuestro algoritmo se basa en buscar la distancia minima (basada en cantidad de saltos), los paquetes que salgan de *nodo 2* va a ir directamente por derecha porque es el camino mas corto a llegar al *nodo 5*, lo mismo ocurre con el *nodo 0* los paquetes van a viajar por derecha, por lo tanto se logra mas equilibrio en los buffer aunque se cuenten otros paquetes, y queda reflejado en la figura 3.

#### Figura 3 - Buffer size de los nodos
![buffersize](./images/algoritmo/caso-1/buffersize.png)

### Caso de Estudio 2
En este caso se verá cuando el *nodo 5* es el unico receptor y todos los demas son generadores en una red con topología de anillo.
Pero vamos a ver el caso generico para n nodos donde tenemos n-1 nodos generadores y un k-esimo nodo receptor.

Ya comentado lo ocurrido en el caso de estudio 1 (con el algoritmo), analizaremos
lo que sucede en este análisis mirando la figura 4. 
#### Figura 4 - Buffer size de los nodos
![buffersize](./images/algoritmo/caso-2/buffersize.png)

Si la cantidad nodos en una red con modelo de anillo es **n** (y los nombres de los nodos van de 0 a n-1).

### Caso n par
Supongamos que el nodo numero k es el receptor entonces el nodo opuesto a k, es decir, el nodo (k+(n/2)) mod n, no va a estar saturado ya que justo los laterales envian en sentido contrario a la dirección a ese nodo.

### Caso n impar
Como la cantidad de nodos es impar ahora no solo va a haber un solo nodo opuesto, si no que van a ser dos que generen paquetes en sentido opuesto por lo tanto esos nodos no se van a saturar.

## Conclusión
Este algoritmo a pesar de ser bastante simple logra escalar a redes bastante más grande que la de anillo que se vió anteriormente, para ser más específico puede ejecutarse en una red de hasta 256 nodos. Como se puede observar dentro de todo resuelva las problemáticas de la capa, sin embargo lo hace con ciertas limitaciones, ya que, busca el camino con menos saltos y es siempre el mismo. Al ser siempre el mismo se saturan todos los buffers de los nodos en dicho camino, por lo tanto tarda más tiempo en procesar los paquetes que le llegan. Si en vez de contar la cantidad de saltos contara el tiempo transcurrido en enviar el paquete a un nodo vecino, cuando el camino "óptimo" se sature va a aumentar el delay, en consecuencia se optaría por otro camino, entonces el camino ya no sería siempre el mismo y cambiaría. Si bien el objetivo del algoritmo es que encuentre el camino más corto para mandar un paquete de un host a otro, modificandolo de la forma antes mencionada tendría como efecto secundario la disminución de la congestión en la red. Por otro lado si no sabe el camino de un host a otro elimina el paquete, esto se podría mejorar utilizando un **Buffer** en Net.cc de tal forma que si no conoce el camino de un host a otro guarde el paquete en dicho buffer y periodicamente revise si se encontró un camino para esos paquetes.
En conclusión el algoritmo es bueno a pesar de las restricciones que posee, si se quisiera implementar para una red que contenga miles o incluso millones de nodos, se pueden aplicar las modificaciones anteriormente mencionadas.
